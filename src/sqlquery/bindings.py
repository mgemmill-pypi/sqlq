import logging
from prompt_toolkit.key_binding import KeyBindings
from . import commands as c
from beercraft import pub

# pylint: disable=W0613


log = logging.getLogger("sqlquery")

kb = KeyBindings()


@kb.add("c-q")
def exit_(event):
    event.app.exit()


@kb.add("c-c")
def _(event):
    " Focus menu. "
    event.app.layout.focus(event.app.root_container.window)


@kb.add("tab")
def next_element(event):
    window = event.app.state.tabs.next()
    event.app.layout.focus(window)


@kb.add("c-r")
def execute_query(event):
    pub.send_message('sql.run')


@kb.add("end")
def move_to_end(event):
    app = event.app
    window = app.layout.current_control
    tabs = app.state.table_tabs

    header = app.state.table_header.buffer
    body = app.state.table_body.buffer

    if window.buffer is body:
        # move body
        end_of_line = body.document.get_end_of_line_position()
        body.cursor_position = end_of_line
        # move header
        end_of_line = header.document.get_end_of_line_position()
        header.cursor_position = end_of_line


@kb.add("home")
def move_to_start(event):
    app = event.app
    window = app.layout.current_control
    tabs = app.state.table_tabs

    header = app.state.table_header.buffer
    body = app.state.table_body.buffer

    if window.buffer is body:
        # move body
        start_of_line = body.document.get_start_of_line_position()
        body.cursor_position = start_of_line
        # move header
        end_of_line = header.document.get_start_of_line_position()
        header.cursor_position = start_of_line


@kb.add("c-right")
def move_right_one_column(event):
    app = event.app
    window = app.layout.current_control
    tabs = app.state.table_tabs

    header = app.state.table_header.buffer
    body = app.state.table_body.buffer

    if window.buffer is body:
        # move body
        current = body.document.cursor_position_col
        move = tabs.next_stop(current) - current
        body.cursor_right(move)
        # move header
        current = header.document.cursor_position_col
        move = tabs.next_stop(current) - current
        header.cursor_right(move)


@kb.add("c-left")
def move_left_one_column(event):
    app = event.app
    window = app.layout.current_control
    tabs = app.state.table_tabs

    header = app.state.table_header.buffer
    body = app.state.table_body.buffer

    if window.buffer is body:
        # move body
        current = body.document.cursor_position_col
        move = current - tabs.prev_stop(current)
        body.cursor_left(move)
        # move header to match
        current = header.document.cursor_position_col
        move = current - tabs.prev_stop(current)
        header.cursor_left(move)
