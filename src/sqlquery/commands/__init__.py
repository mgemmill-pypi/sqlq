from beercraft import subscribe_commands
from . import file
from . import edit
from . import sql
from . import query
from . import connections
from . import viewer
from . import help


def init():
    subscribe_commands(file)
    subscribe_commands(edit)
    subscribe_commands(sql)
    subscribe_commands(query)
    subscribe_commands(connections)
    subscribe_commands(viewer)
    subscribe_commands(help)
