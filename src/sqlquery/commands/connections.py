import logging
from beercraft import pub, expose
from prompt_toolkit.application import get_app
from ..config import new_session, Connection


log = logging.getLogger("sqlquery.commands.connection")


@expose('connection.db_create')
def connection_create(name="", url=""):
    log.info("Add the new db connection: {name}")
    db = new_session()
    db.add(Connection(
        name=name,
        connection_str=url
    ))
    db.commit()

    # send the list of new connections to menu
    connections = db.query(Connection).all()
    pub.send_message("connection.update_menu", connections=connections)


# TODO: this need to be a call to the form and shouldn't be needed here.
@expose('connection.add')
def connection_add():
    pass


@expose('connection.edit')
def connection_update(id=None):
    # called post-editor update
    log.debug(f"Edit connection: {id}")


@expose('connection.delete')
def connection_delete(id=None):
    # called post-prompt
    log.debug(f"Delte connection: {id}")


@expose('connection.selected')
def connection_selected(connection=None):
    app = get_app()
    app.state.current_connection = connection


@expose('exceptions')
def handle_exceptions(error=None):
    log.debug(f"an error occurred: {error}")
