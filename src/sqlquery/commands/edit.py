import logging
from beercraft import pub, subscribe, expose


log = logging.getLogger("sqlquery.commands.edit")


@expose('edit.copy')
def edit_copy():
    pass

@expose('edit.paste')
def edit_paste():
    pass
