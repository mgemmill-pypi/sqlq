import logging
from beercraft import pub, subscribe, expose
from prompt_toolkit.application import get_app


log = logging.getLogger("sqlquery.commands.file")


@expose('file.new')
def file_open():
    pass


@expose('file.open')
def file_open():
    pass


@expose('file.close')
def file_close():
    pass


@expose('file.save')
def file_save():
    app = get_app()
    qry = app.state.current_query
    qry.query.sql = app.state.sql_editor.text
    qry.query.save()


@expose('file.save_as')
def file_save_as():
    pass


@expose('file.exit')
def app_exit():
    app = get_app()
    app.exit()
