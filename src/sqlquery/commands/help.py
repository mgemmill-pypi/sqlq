from beercraft import expose
from prompt_toolkit import HTML
from ..msgbox import show_message


@expose('help.show')
def do_help():
    show_message("Help",
        (
            "Shortcut Keys\n"
            "-------------\n"
            "ctrl-c      Focus to menu.\n"
            "ctrl-f      Focus to file menu.\n"
            "ctrl-e      Focus to edit menu.\n"
            "ctrl-u      Focus to query menu.\n"
            "ctrl-s      Focus to sql menu.\n"
            "ctrl-n      Focus to connection menu.\n"
            "ctrl-r      Run current query.\n"
            "ctrl-q      Exit application.\n"
        )
    )


@expose('help.about')
def do_about():
    show_message("About",
        HTML(
            '<aaa fg="Orange">SqlQuery</aaa>\n\n'
            "A commandline sql querying application."
            "\n\n"
            "©2020 Mark Gemmill."
        )
    )
