import logging
from beercraft import expose
from prompt_toolkit.application import get_app
from beercraft import pub
from .. import util


log = logging.getLogger("sqlquery.commands.query")


def update_title_bar(message, query):
    message.set("favourite", query.favourite * "*")
    message.set("query_file", query.query)
    message.set("query", query.execution_count)
    message.set("connection", query.connection)


@expose('query.open')
def query_open(query=None):
    query.query.open()
    app = get_app()
    app.state.current_query = query
    #  app.state.current_connection = query.connection
    app.state.sql_editor.text = query.query.sql
    update_title_bar(app.state.text_title_msg, query)
    pub.send_message(
        "connection.selected",
        connection=query.connection
    )
