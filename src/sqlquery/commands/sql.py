import logging
from beercraft import pub, subscribe, expose
from datetime import datetime
from prompt_toolkit import HTML
from prompt_toolkit.application import get_app
from beercraft import pub
from .query import update_title_bar
from ..output import DisplayGridHandler
from .. import util


log = logging.getLogger("sqlquery.commands.sql")


@expose('sql.run')
def sql_run():
    app = get_app()
    sql = app.state.sql_editor.text
    qry = app.state.current_query
    conn = app.state.current_connection
    timer = util.Timer()

    # execute the query
    timer.start()
    execution_time = datetime.now()
    headers, results = conn.execute(sql)
    timer.stop()

    # TODO: write result stats or error messages somewhere
    app.state.status_bar_msg.set_message(
        "Rows: {row_count}. Started: {execution_time:%H:%M:%S}. Duration: {duration}.",
        row_count=len(results),
        execution_time=execution_time,
        duration=timer.to_string(),
    )

    dirpath = app.state.results_dir
    title = qry.query.meta["meta"]["title"]

    # output results to display and output file
    results_handler = app.state.result_handler(dirpath, title)
    display_handler = DisplayGridHandler()

    display_handler.write(results, headers)
    app.state.table_header.text = display_handler.header
    app.state.table_body.text = display_handler.text

    app.state.table_tabs = util.TabStops(display_handler.header_underline)

    results_handler.write(results, headers)
    results_handler.open()

    # update query stats
    qry.execution_count += 1
    db = app.state.config_session()
    db.merge(qry)
    db.commit()

    update_title_bar(app.state.text_title_msg, qry)


@expose("sql.run.exceptions")
def handle_sql_run_exceptions(error=None):
    import traceback
    details = traceback.format_exc()
    title="Sql Execution Error!"
    message = HTML(
        '<aaa fg="Beige" bg="DarkRed"> An error occurred while executing the query! </aaa>\n\n'
        f"{error}\n\n"
        f"{details}\n"
    )
    pub.send_message("show_message", title=title, text=message)


@expose("query.analyze")
def query_analyze():
    pass
