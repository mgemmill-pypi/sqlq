import logging
from beercraft import expose
from prompt_toolkit.application import get_app
from .. import output
from .. import state

log = logging.getLogger("sqlquery.commands.connection")


@expose("view.set_handler")
def select_result_handler(name=None):
    log.debug(f"setting default result view handler: {name}")
    state.current.result_handler = output.result_stores.get(name, output.result_stores["Default"])
