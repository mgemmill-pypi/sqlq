from prompt_toolkit import HTML
from prompt_toolkit import print_formatted_text
from prompt_toolkit.formatted_text import to_formatted_text

class StatusBarMessage:
    def __init__(self, msg_fmt, **kwargs):
        self.msg_fmt = msg_fmt
        self.msg_args = kwargs

    def set(self, key, value):
        self.msg_args[key] = value

    def set_message(self, msg, **kwargs):
        self.msg_fmt = msg
        self.msg_args = kwargs

    def __str__(self):
        if any([v is None for v in self.msg_args.values()]):
            return ""
        return self.msg_fmt.format(**self.msg_args)

    def __call__(self):
        return HTML(str(self))
