from datetime import datetime
from pathlib import Path
from importlib.resources import read_text
from sqlalchemy import create_engine, Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
from . import resources
from .query import QueryFile
from .state import current
from . import db


Base = declarative_base()


def init(state):
    engine = create_engine(f"sqlite:///{state.config_db}")
    Base.metadata.create_all(bind=engine)
    Session = sessionmaker(bind=engine)
    state.config_session = Session

    if state.initializing:
        create_default_config(Session, state)


def new_session():
    return current.config_session()


def create_default_config(Session, state):
    session = Session()
    c = Connection(
        name="SqlQuery Config DB", connection_str=f"sqlite:///{state.config_db}",
    )

    session.add(c)

    sample_query = Path(state.query_dir, "config-connections.sql")

    q = Query(
        name="config connection",
        favourite=1,
        filepath=str(sample_query),
        accessed_count=0,
        execution_count=0,
    )

    session.add(q)
    session.commit()

    if not sample_query.exists():
        sample_query.write_text(read_text(resources, sample_query.name))


class BaseTable:  # pylint: disable=R0903
    id = Column(Integer, primary_key=True, autoincrement=True)
    created = Column(DateTime, default=datetime.now)
    modified = Column(DateTime, default=datetime.now, onupdate=datetime.now)


class Connection(Base, BaseTable):
    __tablename__ = "connection"

    name = Column(String(100), nullable=False, unique=True)
    connection_str = Column(String(200))

    @property
    def engine(self):  # pylint: disable=W0201
        if not hasattr(self, "_engine"):
            #  self._engine = create_engine(self.connection_str)
            self._engine = db.get_database(self.connection_str)
        return self._engine

    def execute(self, query):
        #  conn = self.engine.connect()
        with self.engine as conn:
            results = conn.execute(query)
            header = conn.headers
            return header, results

    def __str__(self):
        return self.name


class Query(Base, BaseTable):  # pylint: disable=R0903
    __tablename__ = "query"

    name = Column(String(100), nullable=False, unique=True)
    favourite = Column(Integer)
    filepath = Column(String(500), nullable=False)
    accessed_count = Column(Integer)
    execution_count = Column(Integer)
    connection_id = Column(Integer, ForeignKey(Connection.id))
    connection = relationship(Connection, lazy="immediate")

    @property
    def query(self):  # pylint: disable=W0201
        if not hasattr(self, "_query"):
            self._query = QueryFile(self.filepath)
        return self._query


class QueryResults(Base, BaseTable):  # pylint: disable=R0903
    __tablename__ = "query_result"

    filepath = Column(String(500), nullable=False)

    @staticmethod
    def new_result_set():
        tempfile_ = "blah"
        return QueryResults(filepath=tempfile_)
