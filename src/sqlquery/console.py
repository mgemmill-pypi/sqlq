"""
Usage:
  sqlq  [--initialize]

Options:
  --initialize     Use this the first time.
  -h --help        Show this help message.
  --version        Show version.

"""
import logging
import logging.config
from docopt import docopt
from . import state


__version__ = "0.1.0"


def run():
    args = docopt(__doc__, version=__version__)

    state.init(args["--initialize"])
    logging.config.fileConfig(str(state.current.logging_config))

    from . import main  # pylint: disable=C0415

    app = main.build_application()
    app.run()
