import re
import logging
import platform
from .sqlite import SqliteDatabase
from .postgres import PostgresDatabase

log = logging.getLogger("sqlquery.db")


databases = {
    "sqlite": SqliteDatabase,
    "pgsql": PostgresDatabase,
}


# TODO: pyodbc isn't installing on linux
# does it need to be?
if platform.system() == 'Windows':
    from .odbc import OdbcDatabase
    databases["odbc"] = OdbcDatabase


url_rx = re.compile(r"^(?P<name>[a-z0-9]+)=(?P<cnn>.*)$", re.I)


def get_database(url):
    """
    name://
    """
    m = url_rx.match(url)
    if not m:
        raise Exception("Invalid DB url")

    db_name = m.groupdict()['name']
    db_connection_str = m.groupdict()['cnn']

    log.debug(f"loading database connector: {db_name}")

    database = databases.get(db_name)

    if not database:
        raise Exception("Invalid Database: {db_name}")

    log.debug(f"using connection string: {db_connection_str}")

    return database(db_connection_str)
