import logging
import sqlparse

log = logging.getLogger('sqlquery.db')


class Database:

    def __init__(self, dbapi):
        self.api = dbapi
        self.connection = None
        self.connection_str = None

    def connect(self):
        # creates a new connection
        self.connection = self.api.connect(self.connection_str)
        return self

    def _get_headers(self, cursor):
        self.headers = [attr[0] for attr in cursor.description]

    @classmethod
    def prepare_statement(cls, sql_statement):
        return sqlparse.format(
            sql_statement,
            keyword_case='upper',
            strip_comments=True,
            use_space_around_operators=True,
            #  reindent=True,
            reindent_aligned=True
        )

    @classmethod
    def analyze_sql_statement(cls, sql_statment):
        results = sqlparse.split(sql_statment)
        if len(results) != 1:
            raise Exception("There is more than one sql statement!")

    @classmethod
    def assert_select(cls, sql_statement):
        if sql_statement[:6].upper() != "SELECT":
            log.warning(f"Invalid Select Statement:\n{sql_statement}")
            raise Exception("Non-Select sql statement!")

    def execute(self, sql_statement, *params):
        """
        Execute a single sql select statement and return the results.

        """
        self.analyze_sql_statement(sql_statement)
        clean_statement = self.prepare_statement(sql_statement)
        self.assert_select(clean_statement)

        log.debug(f"Executing Sql Statement:\n{clean_statement}")

        cursor = self.connection.cursor()

        try:
            cursor.execute(clean_statement, params)
            self._get_headers(cursor)
            self.rowcount = cursor.rowcount
            return cursor.fetchall()
        finally:
            cursor.close()

    def close(self):
        if self.connection:
            self.connection.close()

    def __enter__(self):
        return self.connect()

    def __exit__(self, type, value, traceback):
        self.close()
