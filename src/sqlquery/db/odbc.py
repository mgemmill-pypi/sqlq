from .base import Database
import pyodbc


class OdbcDatabase(Database):

    def __init__(self, connection_string):
        super().__init__(pyodbc)
        self.connection_str = connection_string
