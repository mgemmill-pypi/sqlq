from .base import Database
import psycopg2


class PostgresDatabase(Database):

    def __init__(self, connection_string):
        super().__init__(psycopg2)
        self.connection_str = connection_string
