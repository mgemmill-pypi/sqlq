from .base import Database
import sqlite3


class SqliteDatabase(Database):

    def __init__(self, connection_string):
        super().__init__(sqlite3)
        self.connection_str = connection_string
