import datetime
import logging
from asyncio import Future, ensure_future

from prompt_toolkit.application import Application
from prompt_toolkit.application.current import get_app
from prompt_toolkit.completion import PathCompleter
from prompt_toolkit.filters import Condition
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.layout.containers import (
    ConditionalContainer,
    Float,
    HSplit,
    VSplit,
    Window,
    WindowAlign,
)
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.dimension import D
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.layout.menus import CompletionsMenu
from prompt_toolkit.lexers import DynamicLexer, PygmentsLexer
from prompt_toolkit.search import start_search
from prompt_toolkit.styles import Style
from prompt_toolkit.widgets import (
    Button,
    Dialog,
    Label,
    MenuContainer,
    MenuItem,
    SearchToolbar,
    TextArea,
)
from .msgbox import show_message, show_dialog_as_float
from beercraft import pub, expose


log = logging.getLogger('sqlquery')


class ConnectionForm:
    def __init__(self):

        self.future = Future()

        def accept_text(buf):
            get_app().layout.focus(ok_button)
            buf.complete_state = None
            return True

        def accept():
            self.future.set_result({
                "name": self.name.text,
                "url": self.url.text
            })

        def cancel():
            self.future.set_result(None)

        name_label = Label(text="Name:", width=D(preferred=20))
        self.name = name = TextArea(
            multiline=False,
            width=D(preferred=40),
            accept_handler=accept_text,
        )

        name_message = VSplit([Label(text="", width=D(preferred=20)), Label(text="", width=D(preferred=40))])

        url_label = Label(text="Url:", width=D(preferred=20))
        self.url = url = TextArea(
                multiline=False,
                width=D(preferred=40),
                accept_handler=accept_text
        )

        url_message = VSplit([Label(text="", width=D(preferred=20)), Label(text="", width=D(preferred=40))])

        ok_button = Button(text="OK", handler=accept)
        cancel_button = Button(text="Cancel", handler=cancel)

        self.dialog = Dialog(
            title="Connection",
            body=HSplit([
                VSplit([name_label, name]),
                name_message,
                VSplit([url_label, url]),
                url_message,
            ]),
            buttons=[ok_button, cancel_button],
            width=D(preferred=60),
            modal=True,
        )

    def __pt_container__(self):
        return self.dialog


@expose('connection.add')
def do_new_connection(**kwargs):
    async def coroutine():
        dialog = ConnectionForm()

        result = await show_dialog_as_float(dialog)

        try:
            #  show_message("Result", f"{result}")
            if result:
                pub.send_message("connection.db_create", **result)
        except Exception as ex:
            show_message("Error", text=f"{ex}")

    ensure_future(coroutine())
