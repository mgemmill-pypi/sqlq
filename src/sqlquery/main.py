import logging
from beercraft import pub, subscribe, subscribe_commands
from prompt_toolkit import Application
from prompt_toolkit.filters import Condition
from prompt_toolkit.layout.containers import ConditionalContainer
from prompt_toolkit.layout.containers import HSplit
from prompt_toolkit.layout.containers import VSplit
from prompt_toolkit.layout.containers import Window
from prompt_toolkit.layout.containers import WindowAlign
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.lexers import PygmentsLexer
from prompt_toolkit.styles import Style
from prompt_toolkit.widgets import HorizontalLine
from prompt_toolkit.widgets import TextArea
from pygments.lexers.sql import SqlLexer
from .components import StatusBarMessage
from . import menu
from . import msgbox
from . import commands
from . import forms
from . import config
from . import bindings
from . import state
from .util import TabManager


commands.init()
subscribe_commands(forms)
subscribe_commands(menu)
subscribe_commands(msgbox)


log = logging.getLogger("sqlquery")


@subscribe("test.results")
def log_test_results(data):
    log.debug(f"test.results: {data}")


def get_statusbar_right_text():
    return "status right text"


style = Style.from_dict({
    "status": "reverse",
    "shadow": "bg:#440044",
    "table-header": "fg:White bg:Black",
})


def build_application():

    log.debug("starting sqlquery...")
    config.init(state.current)

    state.current.text_title_msg = ttm = StatusBarMessage(
        (
            '<aaa fg="Beige" bg="DarkRed">  {connection}  </aaa>'
            '<aaa fg="Lime" bg="Black"> {favourite} </aaa>'
            '<aaa fg="Silver" bg="Black"> {query:0>4} </aaa>'
            '<aaa fg="Orange" bg="DarkSlateGray">  {query_file}  </aaa> '
        ),
        favourite=None,
        query_file=None,
        query=None,
        connection=None,
    )
    state.current.text_title = text_title = FormattedTextControl(text=ttm)

    state.current.tabs = tabs = TabManager()
    state.current.sql_editor = sql_editor = TextArea(
        scrollbar=True,
        line_numbers=True,
        lexer=PygmentsLexer(SqlLexer),
    )
    state.current.table_body = table_body = TextArea(scrollbar=True, read_only=True, wrap_lines=False)
    state.current.table_header = table_header = TextArea(wrap_lines=False, height=2, style="class:table-header")

    tabs.add_tab(sql_editor)
    tabs.add_tab(table_body)

    state.current.status_bar_msg = sbm = StatusBarMessage(
        "{row_count} rows.", row_count=0
    )

    state.current.status_bar = status_bar = FormattedTextControl(sbm)

    #  search_toolbar = SearchToolbar()

    status_bar = ConditionalContainer(
        content=VSplit(
            [
                Window(status_bar, style="class:status"),
                Window(
                    FormattedTextControl(get_statusbar_right_text),
                    style="class:status.right",
                    width=9,
                    align=WindowAlign.RIGHT,
                ),
            ],
            height=1,
        ),
        filter=Condition(lambda: True),
    )

    title_bar = ConditionalContainer(
        content=VSplit([Window(text_title)], height=2), filter=Condition(lambda: True),
    )


    body = HSplit(
        [
            title_bar,
            sql_editor,
            #  HorizontalLine(),
            table_header,
            table_body,
            #  search_toolbar,
            status_bar,
        ]
    )

    main_menu = menu.build(body)

    layout = Layout(main_menu, focused_element=sql_editor)

    app = Application(
        key_bindings=bindings.kb,
        layout=layout,
        style=style,
        enable_page_navigation_bindings=True,
        mouse_support=True,
        full_screen=True,
    )

    app.root_container = main_menu
    app.state = state.current

    return app
