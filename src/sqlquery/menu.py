import logging
from beercraft import pub, subscribe, expose
from prompt_toolkit.widgets import MenuContainer, MenuItem
from .config import Query, Connection
from . import state
from .forms import do_new_connection


log = logging.getLogger("sqlquery.menu")


def deflag_menu_title(txt):
    return f" {txt[1:]}"


def flag_menu_title(txt):
    return f"*{txt[1:]}"

def strip_menu_title(txt):
    return txt[1:]

def make_menu_title(txt):
    return f" {txt}"


def pub_handler(topic, **kwargs):
    def _pub_handler():
        pub.send_message(topic, **kwargs)
    return _pub_handler


def build_query_menu():
    db = state.current.config_session()
    queries = db.query(Query).filter(Query.favourite > 0).all()

    query_menu = MenuItem(text="Queries ", shortcut="c-u")

    query_menu.children.append(MenuItem(text="Meta", handler=pub_handler('query.meta'),))
    query_menu.children.append(MenuItem(text="----------", disabled=True))

    for query in queries:
        mnu = MenuItem(text=query.name, handler=pub_handler('query.open', query=query),)
        query_menu.children.append(mnu)

    return query_menu


conn_menu = MenuItem(text="Connections ", shortcut="c-n")


@expose("connection.selected")
def set_current_connection(connection=None):
    global conn_menu

    if not connection:
        return
    for mnu in conn_menu.children:
        if mnu.disabled:
            continue
        if connection.name in mnu.text:
            mnu.text = flag_menu_title(mnu.text)
        else:
            mnu.text = deflag_menu_title(mnu.text)


def build_connection_menu():
    global conn_menu
    db = state.current.config_session()
    connections = db.query(Connection).all()

    conn_menu.children.append(MenuItem(text=" New", handler=pub_handler('connection.add'),))
    conn_menu.children.append(MenuItem(text=" ----------", disabled=True))
    #  conn_menu.children.append(MenuItem(text=" Edit", handler=pub_handler('connection.edit'),))

    for connection in connections:
        mnu = MenuItem(
            text=f" {connection.name}",
            handler=pub_handler("connection.selected", connection=connection),
        )
        mnu.children.append(MenuItem(text="Edit", handler=pub_handler('connection.edit', id=connection.id)))
        mnu.children.append(MenuItem(text="Delete", handler=pub_handler('connection.delete', id=connection.id)))
        mnu.id = connection.id
        conn_menu.children.append(mnu)

    return conn_menu


@subscribe('connection.update_menu')
def connection_update_menu(connections=None):
    if not connections:
        log.warn("There are no connection object to update the menu!")
        return

    menus = {strip_menu_title(mnu.text): mnu for mnu in conn_menu.children}

    for connection in connections:
        if connection.name in menus:
            continue
        new_mnu = MenuItem(
            text=make_menu_title(connection.name),
            handler=pub_handler('connection.select', connection=connection)
        )
        new_mnu.id = connection.id
        conn_menu.children.append(new_mnu)


open_menu = MenuItem(text="Viewer ")

@expose("view.set_handler")
def set_current_handler(name=None):
    log.debug(f"updating select view menu option: {name}")
    global open_menu
    for child in open_menu.children:
        if child.id == name:
            child.text = flag_menu_title(child.text)
        else:
            child.text = deflag_menu_title(child.text)


def build_opener_menu(open_menu):

    def build_item(display_name, name):
        mnu = MenuItem(text=display_name)
        mnu.handler = pub_handler("view.set_handler", name=name)
        mnu.id = name
        return mnu

    open_menu.children.append(build_item("*None", "Default"))
    open_menu.children.append(build_item(" Csv",  "Csv"))
    open_menu.children.append(build_item(" Excel","Excel"))
    open_menu.children.append(build_item(" Text", "Text"))
    open_menu.children.append(build_item(" Html", "Html"))

    pub.send_message("view.set_handler", name="Default")

    return open_menu


def build(body):
    log.debug("building menus...")

    return MenuContainer(
        body,
        [
            MenuItem(
                text="File ",
                shortcut="c-f",
                children=[
                    MenuItem(text="New", handler=pub_handler('file.new'),),
                    MenuItem(text="Open", handler=pub_handler('file.open'),),
                    MenuItem(text="Close", handler=pub_handler('file.close'),),
                    MenuItem(text="Save", handler=pub_handler('file.save'),),
                    MenuItem(text="Save As", handler=pub_handler('file.save_as'),),
                    MenuItem(text="Exit", handler=pub_handler('file.exit'),),
                ],
            ),

            MenuItem(
                text="Edit ",
                shortcut="c-e",
                children=[
                    MenuItem(text="Copy", handler=pub_handler('edit.copy'),),
                    MenuItem(text="Paste", handler=pub_handler('edit.paste'),),
                ],
            ),

            build_query_menu(),

            MenuItem(
                text="Sql ",
                shortcut="c-s",
                children=[
                    MenuItem(text="Run", handler=pub_handler('sql.run'),),
                    MenuItem(text="Analyze", handler=pub_handler('sql.analyze'),),
                ],
            ),

            build_connection_menu(),

            build_opener_menu(open_menu),

            MenuItem(
                text="Help",
                children=[
                    MenuItem(text="Help", handler=pub_handler('help.show'),),
                    MenuItem(text="About", handler=pub_handler('help.about'),),
                ],
            ),
        ],
    )
