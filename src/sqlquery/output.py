import os
import platform
import subprocess
from datetime import datetime
from tabulate import tabulate


def normalize_title(query_name):
    return query_name.strip().replace(" ", "-").upper()


class BaseResultsHandler:

    extension = "dat"

    def __init__(self, dirpath, query_name):

        timestamp = datetime.now()
        title = normalize_title(query_name)
        filename = f"{title}-{timestamp:%Y-%m-%d-%H%M%S}.{self.extension}"
        self.filepath = dirpath / filename

    def write(self, results, headers):
        raise NotImplementedError

    def open(self):
        if platform.system() == "Windows":
            os.startfile(self.filepath)  # pylint: disable=E1101
        elif platform.system() == "Darmin":
            subprocess.Popen(["open", self.filepath])
        else:
            subprocess.Popen(["xdg-open", self.filepath])


def calc_row_ids(rows):
    for i in range(1, len(rows) + 1):
        yield f"{i:0>5}"


class TextFileHandler(BaseResultsHandler):

    extension = "txt"

    def write(self, results, headers):
        with self.filepath.open(mode="w") as fh_:
            text = tabulate(results, headers, showindex="always", tablefmt="simple")
            fh_.write(text)


class HtmlFileHandler(BaseResultsHandler):

    extension = "html"

    def write(self, results, headers):
        with self.filepath.open(mode="w") as fh_:
            text = tabulate(results, headers, showindex="always", tablefmt="html")
            fh_.write(text)
            return text


class CsvFileHandler(BaseResultsHandler):

    extension = "csv"

    def write(self, results, headers):

        with self.filepath.open(mode="w") as fh_:

            def write(seq):
                for field in seq:
                    fh_.write(f'"{field}",')
                fh_.write("\n")

            if headers:
                write(headers)

            for row in results:
                write(row)


class ExcelFileHandler(BaseResultsHandler):

    extension = "xlsx"

    def write(self, results, headers):

        with self.filepath.open(mode="w") as fh_:

            def write(seq):
                for field in seq:
                    fh_.write(f'"{field}",')
                fh_.write("\n")

            if headers:
                write(headers)

            for row in results:
                write(row)


class NullHandler:
    def __init__(self, pth, title):
        pass

    def write(self, results, headers):
        pass

    def open(self):
        pass


class DisplayGridHandler:
    def __init__(self):
        self.text = ""
        self.header = ""

    def write(self, results, headers):
        raw_text = tabulate(
            results, headers, showindex=calc_row_ids(results), tablefmt="fancy"
        )
        header_lines = []
        body_lines = []
        body = False
        for line in raw_text.split('\n'):
            if line.startswith('-----'):
                body = True
                header_lines.append(line)
                continue
            if body:
                body_lines.append(line)
            else:
                header_lines.append(line)

        self.text = '\n'.join(body_lines)
        self.header_underline = header_lines[1]
        self.header = '\n'.join(header_lines)

    def open(self):
        pass


result_stores = {
    "Default": NullHandler,
    "Csv": CsvFileHandler,
    "Excel": ExcelFileHandler,
    "Text": TextFileHandler,
    "Html": HtmlFileHandler,
}
