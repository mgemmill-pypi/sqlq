import re
from pathlib import Path
import tomlkit


def parse_query_file(content):
    """
    Represents configuration data stored as part of the query file.

    Query meta data is store at the top of the file:

        /***
        [meta]
        url=
        system=
        fullname=

        [params.param1]
        type=int
        prompt=input
        default=0

        [params.param2]
        type=int
        prompt=input
        default=0

        [substitutions.sub1]
        type=int
        prompt=input
        options=options
        default=0

        ***/

    params: represent parameter info required to build interactive prompts from the user.
    `params`, once collects are not interpolated, but passed to the database execution method
    as parameters.

    substitutions: represent parameters that are substituted in the qry string prior to execution.


    """
    meta_rx = re.compile(
        r"/\*\*\*(?P<meta>[^\*/]*)\*\*\*/(?P<sql>.*)", re.I | re.M | re.S
    )
    m = meta_rx.match(content)
    if not m:
        raise Exception("Invalid query file.")
    meta = tomlkit.parse(m.groupdict()["meta"])
    sql = m.groupdict()["sql"].strip()
    return meta, sql


class QueryFile:
    def __init__(self, filepath):
        self.filepath = Path(filepath)
        self.is_opened = False
        self.content = None
        self.meta = None
        self.sql = None

    def open(self):

        if self.is_opened:
            return

        self.content = self.filepath.read_text()
        self.meta, self.sql = parse_query_file(self.content)

        if "title" not in self.meta["meta"]:
            self.meta["meta"]["title"] = self.filepath.stem

        if "description" not in self.meta["meta"]:
            self.meta["meta"]["description"] = ""

        self.is_opened = True

    def save(self):
        content = ["/***"]
        content.append(tomlkit.dumps(self.meta))
        content.append("***/\n")
        content.append(self.sql)
        content.append("")
        self.filepath.write_text("\n".join(content))

    def __str__(self):
        title = self.meta["meta"]["title"]
        description = self.meta["meta"]["description"]
        return f"{title} / {description}"
