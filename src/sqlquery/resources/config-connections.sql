/***
[meta]
title = "Connection Config DB"
description = "List all sqlquery connections."

***/

-- This is a default query example
-- using sqlqueries own sqlite configuration db.
SELECT
 *
FROM connection
