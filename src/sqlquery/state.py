from importlib.resources import read_text
from pathlib import Path
import appdirs
from . import util
from . import output
from . import resources


class ApplicationState:
    app_name = "SqlQuery"

    def __init__(self):
        self.dirs = dirs = appdirs.AppDirs(self.app_name)
        self.user_data_dir = dta_dir = Path(dirs.user_data_dir)
        self.user_cache_dir = cache_dir = Path(dirs.user_cache_dir)
        self.user_log_dir = log_dir = Path(dirs.user_log_dir)
        self.user_config_dir = cfg_dir = Path(dirs.user_config_dir)
        self.query_dir = dta_dir / "queries"
        self.config_db = cfg_dir / "sqlquery.dat"
        self.logging_config = cfg_dir / "sqlquery.logging.ini"
        self.results_dir = cache_dir / "results_output"
        self.log_file = log_dir / "sqlquery.log.txt"

        self.current_connection = None
        self.results_display_handler = output.DisplayGridHandler


current = None


def init(initializing=False):
    global current
    current = ste = ApplicationState()
    current.initializing = initializing
    util.assert_directory(ste.user_data_dir)
    util.assert_directory(ste.user_cache_dir)
    util.assert_directory(ste.user_log_dir)
    util.assert_directory(ste.user_config_dir)
    util.assert_directory(ste.query_dir)
    util.assert_directory(ste.results_dir)

    if not ste.logging_config.exists():
        log_ini = read_text(resources, "sqlquery.logging.ini")
        ste.logging_config.write_text(log_ini.format(logfile=ste.log_file))
