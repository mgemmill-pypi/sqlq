from datetime import datetime


class TabManager:
    def __init__(self):
        self.controls = []
        self.current = None

    def add_tab(self, ctrl):
        self.controls.append(ctrl)

    def __call__(self, ctrl):
        self.controls.append(ctrl)
        return ctrl

    def _next(self):
        if self.current is None:
            self.current = 0
        else:
            self.current += 1
            if self.current > len(self.controls) - 1:
                self.current = 0
        return self.current

    def next(self):
        return self.controls[self._next()]


class TabStops:

    def __init__(self, header_row):

        def itertabs(src):
            x = 0
            while x >= 0:
                y = src.find(" -", x)
                if y > 0:
                    x = y + 2
                else:
                    x = y
                    continue
                yield y + 1

        self.stops = [stop for stop in itertabs(header_row)]
        self.stops.insert(0, 0)
        self.stops.append(len(header_row) - 1)

    def next_stop(self, current):
        for stop in self.stops:
            if stop > current:
                return stop
        return self.stops[-1]

    def prev_stop(self, current):
        for stop in reversed(self.stops):
            if stop < current:
                return stop
        return self.stops[0]


def assert_directory(directory):
    directory.mkdir(parents=True, exist_ok=True)


class Timer:
    def __init__(self):
        self._start_time = None
        self._end_time = None

    def start(self):
        self._start_time = datetime.now()

    def stop(self):
        self._end_time = datetime.now()

    @property
    def duration(self):
        return self._end_time - self._start_time

    @property
    def elapsed_seconds(self):
        return self.duration.total_seconds()

    @property
    def elapsed_minutes(self):
        return self.elapsed_seconds / 60

    def to_string(self):
        if self.elapsed_minutes < 1.0:
            return f"{self.elapsed_seconds:.4} seconds"
        return f"{self.elapsed_minutes:.4} minutes"
