header = "----- ----- ----- --------- ---------- ---------------- ---"

def itertabs(src):
    x = 0
    while x >= 0:
        y = src.find(" ", x)
        if y > 0:
            x = y + 1
        else:
            x = y
            continue
        yield y

#  import pdb;pdb.set_trace()
tabs = [stop for stop in itertabs(header)]

print(tabs)
