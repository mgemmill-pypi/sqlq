from sqlquery.db.base import Database



def test_prepare_statement():

    stmt = Database.prepare_statement("""
/*
domwer kind of commment..
*/
select A.moore as MOORE
-- and forget this...
from schema.sometable as A where
A.foo = 'no' AND A.bar > 2


""")

    assert stmt == (
        "SELECT A.moore AS MOORE\n"
        "  FROM schema.sometable AS A\n"
        " WHERE A.foo = 'no'\n"
        "   AND A.bar > 2"
    )


def test_prepare_statement():

    stmt = Database.prepare_statement("""
/*
domwer kind of commment..
*/
select
  A.moore as MOORE,
  A.foo AS ACTIVE
-- and forget this...
from schema.sometable as A
where
  A.foo = 'no' AND
  A.bar > 2

""")

    assert stmt == (
        "SELECT A.moore AS MOORE,\n"
        "       A.foo AS ACTIVE\n"
        "  FROM schema.sometable AS A\n"
        " WHERE A.foo = 'no'\n"
        "   AND A.bar > 2"
    )


class DummyDBAPI:
    def __init__(self):
        self._results = (('one', 'two'), ('three', 'four'))
        self._rowcount = 2
        self._description = [('COLA', 0),  ('COLB', 0)]
        self._closed = False

    def connect(self, connection_str):
        self.connection = DummyDBAPI()
        return self.connection

    def cursor(self):
        self._cursor = DummyDBAPI()
        return self._cursor

    @property
    def description(self):
        return self._description

    @property
    def rowcount(self):
        return self._rowcount

    def execute(self, sql, params):
        pass

    def fetchall(self):
        return self._results

    def close(self):
        self._closed = True


def test_database_api():
    dbapi = DummyDBAPI()

    with Database(DummyDBAPI()) as db:
        results = db.execute("SELECT * FROM DB")
        assert results == dbapi._results
        assert db.rowcount == 2
        assert db.headers == ['COLA', 'COLB']
        assert db.connection._cursor._closed == True
