from sqlquery import query

query_file_text = """/***
[meta]
url="a url"
system="system name"
fullname="full query name"

***/

SELECT
 *
FROM MYTABLE AS A
WHERE
  A.NAME LIKE '%THIS%'

"""


def test_meta_data():
    meta, sql = query.parse_query_file(query_file_text)

    assert meta
    assert meta["meta"]["url"] == "a url"
    assert meta["meta"]["system"] == "system name"
    assert meta["meta"]["fullname"] == "full query name"

    assert sql == (
        "SELECT\n" " *\n" "FROM MYTABLE AS A\n" "WHERE\n" "  A.NAME LIKE '%THIS%'"
    )
