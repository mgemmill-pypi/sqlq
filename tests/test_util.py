from sqlquery.util import TabStops



def test_tabstops_one():
    #      0    5     11    17    23    29  34
    src = "----- ----- ----- ----- ----- -----"
    stops = TabStops(src)

    assert stops.next_stop(0) == 6
    assert stops.next_stop(6) == 12
    assert stops.next_stop(12) == 18
    assert stops.next_stop(18) == 24
    assert stops.next_stop(24) == 30
    assert stops.next_stop(30) == 34

    assert stops.prev_stop(34) == 30
    assert stops.prev_stop(30) == 24
    assert stops.prev_stop(24) == 18
    assert stops.prev_stop(18) == 12
    assert stops.prev_stop(12) == 6
    assert stops.prev_stop(6) ==  0

def test_tabstops_two():
    #      0    5     11    17    23    29  34
    src = "----  ----  ----  ----  ----  -----"
    stops = TabStops(src)

    assert stops.next_stop(0) == 6
    assert stops.next_stop(6) == 12
    assert stops.next_stop(12) == 18
    assert stops.next_stop(18) == 24
    assert stops.next_stop(24) == 30
    assert stops.next_stop(30) == 34

    assert stops.prev_stop(34) == 30
    assert stops.prev_stop(30) == 24
    assert stops.prev_stop(24) == 18
    assert stops.prev_stop(18) == 12
    assert stops.prev_stop(12) == 6
    assert stops.prev_stop(6) ==  0
